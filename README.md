# TP intro à la preuve deduc

Le travail à rendre pour l'UE intro à la preuve déductive en M1 MPRI à Paris-Saclay

## Section Why3
Tous les exercices dans un seul fichier + la question bonus "4 colors Dutch flag".

Tous les exercices sont prouvés. Certains nécessitent une manipulation avec split and prove et/ou prove 1000/5000 steps.



## Section Frama-c ACSL
