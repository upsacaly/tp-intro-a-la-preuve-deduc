/* ****************************************************************************
                              RECHERCHE POINTÉE

   L'énoncé est introduit sous la forme de commentaires C. Les différents
   morceaux de programme et de spécification à considérer sont mentionnés en
   programmation littéraire :

              https://en.wikipedia.org/wiki/Literate_programming

   ************************************************************************* */

/* ---------------------------------------------------------------------------
   La fonction [find_ptr] ci-dessous prend en entrée deux pointeurs [p] et [q]
   pointant vers un même bloc mémoire tels que [p] soit avant [q]. Si la valeur
   [v] est présente dans ce bloc entre [p] (inclus) et [q] exclus, alors la
   fonction retourne un pointeur vers l'endroit où cette valeur est
   présente. Sinon, la fonction retourne [q].
   ------------------------------------------------------------------------- */

/* ---------------------------------------------------------------------------
   Question 1 : définir le corps du prédicat [found] de manière à satisfaire sa
   spécification informelle.
   ------------------------------------------------------------------------- */
   
/* [found(start, last, v)] est vrai si et seulement s'il existe un pointeur
   entre [start] (inclus) et [last] (exclus) appartenant au même bloc mémoire
   que [start] et pointant vers [v]. */
/*@ predicate found(unsigned char *start, 
  @                 unsigned char *last, 
  @                 unsigned char v)
  @ = 
  @   \exists unsigned char * i ; start <= i < last &&  *i == v && ( \base_addr(start) == \base_addr(last) );
  @ */

/* ---------------------------------------------------------------------------
   Question 2 : spécifier (en utilisant des comportements) et prouver la
   fonction [find_ptr], y compris sa terminaison et l'absence d'erreurs à
   l'exécution.
   ------------------------------------------------------------------------- */

/*@ requires \valid ( p ) && \valid_read ( q );
   @ requires \base_addr(p) == \base_addr(q);
   @ assigns *p;
   @ ensures \result >=  p && \result <= q ;
   @behavior exists:
      assumes found (p, q, v) ;
      ensures *(\result)== v;
   @behavior notexists:
      assumes !found (p, q, v) ;
      ensures \result== q;
   @complete behaviors;
   @disjoint behaviors;
*/
unsigned char *find_ptr(unsigned char *p, unsigned char *q, unsigned char v) {
  /*@loop assigns p;
  @loop invariant p <= q;
  @loop invariant ! found ( \at(p,LoopEntry) , p, v );
  @loop variant q -p;
  */
  for (; p < q; p++) 
    if (*p == v) break;
  return p;
}
